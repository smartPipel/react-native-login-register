import React, {
  Component
} from 'react';
import {
  createAppContainer, StackActions, NavigationActions
} from 'react-navigation';
import {
  createStackNavigator, HeaderTitle
} from 'react-navigation-stack';
import { Ionicons } from '@expo/vector-icons';
import {
  View,
} from 'react-native';

import HomeScreen from './src/view/HomeScreen';
import RegisterScreen from './src/view/RegisterScreen';
import LoginScreen from './src/view/LoginScreen';
import UserActivities from './src/view/tabScreen/activities';
import UserProfile from './src/view/tabScreen/profile';
import { removeUser } from './src/controller/databasesHelper';
import currentUser from './src/view/currentUser';


const Navigasi = createStackNavigator({
  CurrentUser:{
    screen:currentUser,
    navigationOptions: {
      title: null,
      headerStyle:{
        elevation:0
      }
    }
  },
  Home: {
    screen: HomeScreen, 
    navigationOptions: ({navigation}) => ({
      title: 'Explore',
      headerStyle:{
        backgroundColor: "#222831",
        elevation: 0,
      },
      headerTintColor:"#fff",
      headerLeft:()=>{
        return <Ionicons name="ios-log-out" color="white" style={{fontSize: 20 , marginLeft:20, marginRight: 50}} 
        onPress={
          (async () => {
              await removeUser();

              const reset = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({routeName:'Login'})]
              })
              
  
              navigation.dispatch(reset)
          }).bind(this)
      }
        />
      },
      headerRight:()=>{
        return <View style={{marginRight: 20, flexDirection:"row"}}>
          <Ionicons name="ios-notifications-outline" color="white" style={{fontSize: 20 }} />
          <Ionicons name="ios-search" color="white" style={{fontSize: 20 , marginLeft:20}} />
          </View>
      },
      headerTitleStyle:{
        marginLeft: 70
      }
    })
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      title: "",
      headerStyle:{
        backgroundColor: "#222831",
        elevation: 0,
      },
      headerTintColor: "#fff",
      headerTitleStyle:{
        marginLeft: 70
      }
    }
  },
  Register: {screen: RegisterScreen},
  UserAct: {screen: UserActivities},
  UserProfile: {screen: UserProfile},
},
  {
    initialRouteName: "CurrentUser",
  }
);


export default createAppContainer(Navigasi);