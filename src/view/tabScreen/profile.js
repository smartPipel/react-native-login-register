import React, {Component} from 'react';
import { View, StyleSheet, Dimensions, Text, Image } from 'react-native';
import HomeScreen from '../HomeScreen';
import  styles from '../../styles/profileStyles';



export default class UserProfile extends Component{

    constructor(props){
        super(props)

    }
    render() {
        const { navigation } = this.props;

        return(
            <View style={styles.homeContainer}>
                <View>
                    <Image source={require('../../../assets/Image/tanjirou.jpg')} style={styles.cardContainer} />
                     <View style={styles.overlay}>
                        <Image source={require('../../../assets/Image/tejina.jpg')} style={styles.avatar} />
                        <View>
                            <Text style={styles.usernameText}>{this.props.user.nama}</Text>
                            <Text style={styles.emailText}>{this.props.user.email}</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: "row" }} >
                    <View style={{marginTop:15, marginRight: 15}}>
                        <Image source={require('../../../assets/Image/profile.jpg')} style={styles.cardContainerView} />
                        <View style={styles.overlayView}>
                            <View>
                                <Text style={styles.viewMenuText}>Followers</Text>
                                <Text style={styles.viewNumber}>1897</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop:15}}>
                        <Image source={require('../../../assets/Image/profile.jpg')} style={styles.cardContainerView} />
                        <View style={styles.overlayView}>
                            <View>
                                <Text style={styles.viewMenuText}>Following</Text>
                                <Text style={styles.viewNumber}>50</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

