import * as React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    StatusBar,
    Image
} from 'react-native';
import UserActivities from './tabScreen/activities';
import UserProfile from './tabScreen/profile';
import { SceneMap, TabView, TabBar } from 'react-native-tab-view';
import  styles from '../styles/profileStyles';
import { getUser } from '../controller/databasesHelper';


const initialLayout = { width: Dimensions.get('window').width };


export default class HomeScreen extends React.Component{
        
    constructor(props){
        super(props)

        this.state ={
            index : 0,
            routes : [
                {key: 'profile', title: 'profile'},
                {key: 'activities', title: 'activities'}
            ],
            user: {},
        }
        
        getUser().then(user=>{
            this.setState({user})
        })


    }



    _handelIndexChange = index => this.setState({ index })



    _renderHeader = props => <TabBar {...props} indicatorStyle={{ backgroundColor:"#ff7bb0", height: 3, marginVertical: 4, width:150, marginStart:10, borderRadius:10 }} style={styles.tabBar}/>

    _renderScene = ({ route }) => {
        // alert(JSON.stringify(this.state.user))

        switch (route.key) {
          case 'profile':
            return <UserProfile user={this.state.user} />;
                
          case 'activities':
            return <UserActivities />;
          default:
            return null;
        }
    };
     render(){
        
        return (
            <TabView
                navigationState={this.state}
                renderScene={this._renderScene.bind(this)}
                renderTabBar= {this._renderHeader}
                onIndexChange={this._handelIndexChange}
                initialLayout={initialLayout}
                
            /> 
        );
     }
    }
